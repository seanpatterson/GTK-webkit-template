using GLib;
using Gtk;
using WebKit;

public class Browser : Window {
    private const string URL = "http://google.com";

    public Browser() {

        this.window_position = WindowPosition.CENTER;
        this.set_default_size(200, 200);
        this.destroy.connect(Gtk.main_quit);

        Gtk.HeaderBar headerbar = new Gtk.HeaderBar();
        headerbar.show_close_button = true;
        headerbar.title = "Browser";
        this.set_titlebar(headerbar);

        Gtk.Button button = new Gtk.Button.with_label ("Click me");
        headerbar.pack_end(button);

        WebView view = new WebView();
        view.open(Browser.URL);

        ScrolledWindow scrolled_window = new ScrolledWindow(null, null);
        scrolled_window.set_policy(PolicyType.AUTOMATIC, PolicyType.AUTOMATIC);
        scrolled_window.add(view);

        this.add(scrolled_window);
    }

    public static int main(string[] args) {
        Gtk.init(ref args);

        Browser browser = new Browser();
        browser.show_all();

        Gtk.main();

        return 0;
    }
}