A basic WebKit app for GTK built using Vala. 
To compile and run run: valac --vapidir=. --pkg gtk+-3.0 --pkg webkitgtk-3.0 main.vala; ./main
